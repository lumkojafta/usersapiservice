﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UserDetails.DAL;
using UserDetails.DTO;
using UserDetails.Models;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace UserDetails.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserDetailController : ControllerBase
    {
        private readonly DataContext _repository;
        public UserDetailController(DataContext repository) {
            _repository = repository;
        }
        // GET: api/<UserDetailController>
        [HttpGet]
        public  ActionResult<IEnumerable<UserDetail>> Get()        {
           
            var entry = GetAll();
            return Ok(entry);
        }

        // GET api/<UserDetailController>/5
        [HttpGet("{id}")]
        public ActionResult<IEnumerable<UserDetail>> GetDetail(int id)
        {
            var entry = GetAll().Where(x=>x.Id == id);
            return Ok(entry);
        }

        // POST api/<UserDetailController>
        [HttpPost]
        public ActionResult Post([FromBody] UserDelailDto value)
        {
            var dataEntry = new UserDetail();
            if (ModelState.IsValid)
            {

                dataEntry = new UserDetail          
                {

                    ////  Id = value.Id,
                    First_Name = value.First_Name,
                    Last_Name = value.Last_Name,
                    Date_Of_Birth = value.Date_Of_Birth,
                    Email_Address = value.Email_Address,
                    Address = value.Address,
                    City = value.City,
                    Country = value.Country,
                    Zip_Code = value.Zip_Code                   
                    
                };
                _repository.UserDetail.Add(dataEntry);
                _repository.SaveChangesAsync();

            }
            else {
                return BadRequest(ModelState);
            }
            return Ok(dataEntry);
        }

        // PUT api/<UserDetailController>/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/<UserDetailController>/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }

        private IQueryable<UserDetail> GetAll() {

            return _repository.UserDetail.AsQueryable();
        }
    }
}
